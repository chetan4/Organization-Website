module WordMethods
  def word_count
    self.split(' ').count + 1
  end
end
