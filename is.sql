-- MySQL dump 10.13  Distrib 5.1.54, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: is_phase2_prod
-- ------------------------------------------------------
-- Server version	5.1.54-1ubuntu4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `active_admin_comments`
--

DROP TABLE IF EXISTS `active_admin_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `active_admin_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `resource_id` int(11) NOT NULL,
  `resource_type` varchar(255) NOT NULL,
  `author_id` int(11) DEFAULT NULL,
  `author_type` varchar(255) DEFAULT NULL,
  `body` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `namespace` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_admin_notes_on_resource_type_and_resource_id` (`resource_type`,`resource_id`),
  KEY `index_active_admin_comments_on_namespace` (`namespace`),
  KEY `index_active_admin_comments_on_author_type_and_author_id` (`author_type`,`author_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `active_admin_comments`
--

LOCK TABLES `active_admin_comments` WRITE;
/*!40000 ALTER TABLE `active_admin_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `active_admin_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_users`
--

DROP TABLE IF EXISTS `admin_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL DEFAULT '',
  `encrypted_password` varchar(128) NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) DEFAULT NULL,
  `last_sign_in_ip` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_admin_users_on_email` (`email`),
  UNIQUE KEY `index_admin_users_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_users`
--

LOCK TABLES `admin_users` WRITE;
/*!40000 ALTER TABLE `admin_users` DISABLE KEYS */;
INSERT INTO `admin_users` VALUES (1,'admin@idyllic-software.com','$2a$10$y3MoalTy.r.JXq.F4r.QCeoYNAYy/W8SORHfMRlAlM3IMOIwzWVka',NULL,NULL,NULL,9,'2011-10-20 04:23:48','2011-10-19 13:48:01','61.12.126.211','61.12.126.211','2011-10-17 06:37:19','2011-10-20 04:23:48');
/*!40000 ALTER TABLE `admin_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brief_bios`
--

DROP TABLE IF EXISTS `brief_bios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brief_bios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `maverick_id` int(11) DEFAULT NULL,
  `professional_experience` text,
  `purpose_of_life` varchar(255) DEFAULT NULL,
  `topics_of_interest` text,
  `why_work_with_me` text,
  `favorite_quote` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `education` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brief_bios`
--

LOCK TABLES `brief_bios` WRITE;
/*!40000 ALTER TABLE `brief_bios` DISABLE KEYS */;
INSERT INTO `brief_bios` VALUES (1,1,'* Engagement Engineer @ CenterPost Communications, Chicago, IL\r\n* Application Developer @ Classified Ventures LLC , Chicago, IL\r\n* Team Leader @ Norvax Inc, Chicago, IL\r\n* Team Leader @ Sears & Roebuck, Chicago, IL\r\n* Offshore Lead @ Productive Edge LLC,  Pune, India\r\n* Software Manager @ Datastraits, Pune, India','Influence lives','* Lean Software Development & Quick time to market.\r\n* Simplify software development\r\n* Create a platform(not a company) for allowing people to grow as fast as they can.\r\n* Self discovery and living in complete awareness.\r\n','* Engage as a partner not as a service provider\r\n* Pragmatic thinking\r\n* Do not shy away to say \"Your baby is ugly\"\r\n* Singular engagement mission - Realize your project','Be the change that you want to see in the world - Mohandas Gandhi ','2011-10-17 09:15:25','2011-10-20 06:05:08','* MS Software Engineering - DePaul University, Chicago, IL\r\n* BE Computer Engineering - Mumbai University, India\r\n* Sun Certified Java Programmer\r\n* Sun Certified Web Developer\r\n* .NET certification- DePaul University, Chicago, IL'),(2,3,'### Idyllic Software\r\n**Conjurer**\r\n_December 2009 - Current_\r\n\r\n`ruby` `ruby-on-rails` `sinatra` `padrino` `jquery`\r\n\r\nAs a founder member my primary responsibility is to lead the development of various projects, ensuring code and design quality, timely delivery and client satisfaction. Crafting the web is something I specialize in using Ruby and Ruby on Rails here. My creativity plays an important role in product design and problem solving that we encounter on every typical day. I\'m an advisor and point of contact for some of the clients. Mentoring junior developers and software craftsmenship evangelism is also something I do very passionately.\r\n\r\n\r\n### Persistent Systems Limited\r\n**Software Engineer**\r\n_January 2008 - December 2009_\r\n\r\n`ruby` `ruby-on-rails` `ruby-on-rails-plugin-development` `ajax` `javascript` `oop` `solution-design`\r\n\r\nMy primary responsibility was to design and code web applications in Ruby and Ruby on Rails for variety of clients belonging to several domains. I have been involved in complete development life-cycle of products and solutions ranging from social networks, reporting tools, REST APIs to rails plugin development and spiders.\r\n\r\nI played a key role in making team members understand the importance of writing great code and caring about their craft. This involved promoting reading of blogs and books, code reviews, tech-talks, mentoring juniors. I evangelized agile in a strictly SDLC kind of organization.','Influence lives','- Programming\r\n- Travelling\r\n- Adventure Sports\r\n- Photography\r\n- Food (Cooking as well as appreciation)',' - I **love** to write code. My current programming interests include [Ruby](http://stackoverflow.com/search?q=user:45942+[ruby]&tab=votes) and [Ruby on Rails](http://stackoverflow.com/search?q=user:45942+[ruby-on-rails]&tab=votes).\r\n - I **live** in objects. I see them everywhere. OOP comes to me naturally. \r\n - I **dig** agile. I don\'t defend it.\r\n - I **hate** duplications. Be it code, design, data and most importantly effort.\r\n - I am **obsessed** with software craftsmanship and writing (and letting others write) only beautiful code.\r\n - I **value** creativity, geekism, smartness and humor.\r\n - I **like** to work with a team of smart, pragmatic people focused on solving real world problems. \r\n\r\nI\'m a back-end developer who likes user interface. I have a good product acumen and a sense for product design. I work mostly with a skill I don\'t hold a degree in - *common sense*. I\'m known for my *passion for programming*, *clean design* and *hard work*. \r\n\r\n__[![github](http://jmoiron.net/media/icons/github-color.png \"Github\")](http://github.com/chirantan) [![twitter](http://imaginepeace.com/wp-content/themes/iblogpro/images/icons/ico-twitter.png \"Twitter\")](http://twitter.com/chirantan) [![skype](http://www.gogrok.com/en/images/Img_Download/SkypeBlue.png \"My Skype ID - chirantan.rajhans\")](skype:chirantan.rajhans?call)__','The only boundaries are the ones in your head.','2011-10-17 10:01:46','2011-10-19 12:57:02',NULL),(3,12,'-  Mayavi : Idyllic Software\r\n-  Graphics designer : Redchip Solution\r\n-  Graphics designer : Pune IT Labs\r\n-  Graphics designer : Promark Infotech\r\n','- Life is a camera, click the best out of it :). Want to be the finest designer in Graphics World before I die','- Knowing software\'s and trends in design world\r\n- Sketching & Digital painting in Photoshop\r\n- Surfing on the internet\r\n- Playing flute & a few dance movements\r\n- Live life with a fighting spirit','My self motivation and enjoyment in work allows people to delegate design work to me without bothering to follow up. I am very open to design suggestions that come from others and am able to incorporate in my designs without making the design my propriety. ','Get inspired but be yourself.','2011-10-17 10:13:31','2011-10-19 12:53:03',NULL),(4,14,'Brute Force @ Idyllic-software\r\n\r\n * In one line - I care about the whole project.\r\n\r\n![Alt text](http://www.rustybrick.com/blog/archives/q-a-comic.gif)\r\n','That is a million dollar question to me and I am yet to find the answer.','- Automation testing tools like selenium\r\n- Identifying ways to speed up smoke tests of quick features\r\n- Resources and Dependencies Model\r\n- Black box, Sanity & Regression testing\r\n- Sketching & music\r\n','- I care about the whole project\r\n- I know how to find the hidden mis-use, ex: no one still knows how to embed an image in this section of our website.\r\n- Strong attention to detail\r\n- Good listener & have a \"test to break\" attitude\r\n- Friendly & you will know more when you work with me\r\n\r\n','If you canot do great things, do the small things greatly! ','2011-10-17 11:23:32','2011-10-19 10:55:37',NULL),(5,6,'* Software Engineer @ RedEastAppLabs\r\n* Software Consultant @ Ascra Technologies\r\n* Sr. Rails Developer @ Idyllic Software\r\n','To make difference in someone\'s life','* Ruby on Rails, Javascript, html and css\r\n* To learn new things and improving my productivity\r\n* Sports, Music\r\n','My team spirit, timeliness & sticking to commitments.','Try & try & try & try ..... till you succeed','2011-10-17 11:31:14','2011-10-19 12:29:27',NULL),(6,5,'* Software Engineer at Lionbridge Technologies\r\n* Technology Lead at Ibloom Technologies\r\n* Innovator at Sapnasolutions','Scream \"This is Sparta\" at the most inappropriate moments.','* Behavioral Conditioning\r\n* Real Time technologies\r\n* XMPP\r\n* RabbitMQ\r\n* NodeJS\r\n* Ruby\r\n* Javascript\r\n* IOS\r\n* Pizzas\r\n\r\n\r\n\r\n','I\'ll share my coke and introduce you to Pink Floyd','http://www.youtube.com/watch?v=4e0n7vTLz1U','2011-10-17 12:54:18','2011-10-18 12:26:27',NULL),(7,9,'* Idyllic Software - Rails Developer \r\n* Corewire research labs  - Software Developer\r\n*  Betterlabs - Software Developer\r\n* Software Source - Junior Abap consultant','To live the life at its fullest without any complains.With richard branson making space travel possible, that would be one thing that i would really love to do.','Sports\r\nMusic','My humour and open attitude make people easily connected and comfortable with me.','Choose a job you love and you will never have to work a day in your life.','2011-10-17 13:19:48','2011-10-18 12:25:41',NULL),(8,8,'* Idyllic Software Pvt. Ltd. (Fungineer)\r\n* Sapna Solutions (Innovator)\r\n* Sprit Sports Pvt. Ltd. - An Essel Group Company (Software Engineer Core)\r\n* BetterLabs (Software Engineer Core)\r\n* Avibha IT Solution (Industrial trainee)\r\n','Live life to its fullest & spread the happiness around.','- Ruby on rails & the Web world\r\n- Learning new ways to improving my productivity. Getting things done by David Allen is one my latest finds!\r\n- Food business & Rajshri movies\r\n','- Quick turn around and make myself available when you need me on the eleventh hour\r\n- My sense of humor will keep the development process enjoyable\r\n- When I do not know the answer, I know how to find it','Be positive, and things will automatically fall in place.','2011-10-18 04:18:03','2011-10-19 10:13:55',NULL),(9,7,'### Idyllic-Software\r\n\r\n**CodeNinja** - _August 9 2010 - Current_\r\n\r\n### KQInfotech\r\n\r\n**Intern*** - _December 14th 2009 - July 26th 2010_\r\n','Do what you love.','- Coding\r\n- Reading\r\n- Photography\r\n- Coffee\r\n- Music (A. R. Rahman)\r\n\r\n\r\n','- I am passionate when it comes to application programming\r\n- I stay up to date with current trends I thrive on trying out cutting edge tools\r\n- I gel well with diverse teams','Don\'t live to geek, geek to live.','2011-10-18 18:29:56','2011-10-19 12:47:21',NULL);
/*!40000 ALTER TABLE `brief_bios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client_inquiries`
--

DROP TABLE IF EXISTS `client_inquiries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_inquiries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `message` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_inquiries`
--

LOCK TABLES `client_inquiries` WRITE;
/*!40000 ALTER TABLE `client_inquiries` DISABLE KEYS */;
/*!40000 ALTER TABLE `client_inquiries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_inquiries`
--

DROP TABLE IF EXISTS `job_inquiries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_inquiries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `git_hub_url` varchar(255) DEFAULT NULL,
  `aspirations` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `resume_file_name` varchar(255) DEFAULT NULL,
  `resume_content_type` varchar(255) DEFAULT NULL,
  `resume_file_size` int(11) DEFAULT NULL,
  `resume_updated_at` datetime DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_inquiries`
--

LOCK TABLES `job_inquiries` WRITE;
/*!40000 ALTER TABLE `job_inquiries` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_inquiries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mavericks`
--

DROP TABLE IF EXISTS `mavericks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mavericks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL DEFAULT '',
  `encrypted_password` varchar(128) NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) DEFAULT NULL,
  `last_sign_in_ip` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `publish` tinyint(1) DEFAULT '0',
  `sort_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_mavericks_on_email` (`email`),
  UNIQUE KEY `index_mavericks_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mavericks`
--

LOCK TABLES `mavericks` WRITE;
/*!40000 ALTER TABLE `mavericks` DISABLE KEYS */;
INSERT INTO `mavericks` VALUES (1,'jparekh@idyllic-software.com','$2a$10$kslnoHjmkVkVxbS9VW20dOc1.V8VhH7YoLQs5ZLrRLhuFmE9JWgC2',NULL,NULL,NULL,12,'2011-10-20 05:40:43','2011-10-20 04:47:11','61.12.126.211','61.12.126.211','2011-10-17 06:37:19','2011-10-20 05:40:43','Jinesh Parekh',1,NULL),(2,'brian@startupguru.com','$2a$10$MfS0WHlW8V3I5Q0LCtxPBeplmUCCczV3M8jxSVzSAcLSJ34q7M14G',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'2011-10-17 06:37:19','2011-10-17 06:37:19','Brian Tsuchia',1,NULL),(3,'chirantan@idyllic-software.com','$2a$10$Ce1lnnXfCQw82TnENxl.huGpmGqNSvn1achDLo4u1nilmSLZX20lK',NULL,NULL,NULL,5,'2011-10-19 13:10:20','2011-10-18 17:47:20','61.12.126.211','123.236.183.143','2011-10-17 06:37:19','2011-10-19 13:10:20','Chirantan Rajhans',1,NULL),(4,'vignesh@idyllic-software.com','$2a$10$EkLTVL5.T83bN0rjWy3j9esZNTH3Jva2AwKufp.85C4s9jCjSK316',NULL,NULL,NULL,3,'2011-10-18 18:48:06','2011-10-18 17:27:01','123.236.183.143','122.170.23.1','2011-10-17 06:37:19','2011-10-18 18:48:06','Vignesh Shanbhag',1,NULL),(5,'siddharth@idyllic-software.com','$2a$10$DsOg/q2k8zJ1CNm2mrCX7.X/yZjYVTsht0D5Zem3s6OGy37.GV6Xi',NULL,NULL,NULL,3,'2011-10-19 13:19:03','2011-10-19 12:56:26','120.61.35.119','120.61.35.119','2011-10-17 06:37:19','2011-10-19 13:19:03','Siddharth Ravichandran',1,NULL),(6,'prem@idyllic-software.com','$2a$10$okkO43TX7dwJBvdvbFI1p.RmI94hCvUqb0Zce6uRDnqpO8qS7if3.',NULL,NULL,NULL,5,'2011-10-19 13:07:12','2011-10-19 12:21:45','61.12.126.211','61.12.126.211','2011-10-17 06:37:20','2011-10-19 13:07:12','Prem Kumar',1,NULL),(7,'santosh@idyllic-software.com','$2a$10$5KVKwFgMHLJPMqub5ZHjF.m2y/OhKmUo00pkhLPEoMLMYKme4H182',NULL,NULL,NULL,4,'2011-10-18 18:49:19','2011-10-18 17:49:53','123.236.183.143','123.236.183.143','2011-10-17 06:37:20','2011-10-18 18:49:19','Santosh Wadghule',1,NULL),(8,'chetan@idyllic-software.com','$2a$10$Z/yPxL0KxBlMRZhkDjXXd.PxJhVLFSk6coLE9kuyw6JcC06qksIkG',NULL,NULL,NULL,7,'2011-10-20 04:16:02','2011-10-19 13:35:56','61.12.126.211','61.12.126.211','2011-10-17 06:37:20','2011-10-20 04:16:02','Chetan Thakkar',1,NULL),(9,'aniket@idyllic-software.com','$2a$10$FlFq06qy8j5TqgjUkOWCI.fvjkP6UHmRqR9Wbe7p1X1NrDVxDwn82',NULL,NULL,NULL,4,'2011-10-19 12:47:07','2011-10-19 05:45:45','61.12.126.211','61.12.126.211','2011-10-17 06:37:20','2011-10-19 12:47:07','Aniket Joshi',1,NULL),(10,'rtdp@rainingclouds.com','$2a$10$8K5H9E2YB9Qs8bYJjqmbjeAysufbuf.2FgtIjAuY9wA5R1wPItM3G',NULL,NULL,NULL,3,'2011-10-19 12:38:09','2011-10-19 12:18:56','61.12.126.211','61.12.126.211','2011-10-17 06:37:22','2011-10-19 12:38:09','Ratnadeep Deshmane',1,NULL),(11,'saji@idyllic-software.com','$2a$10$ZOMXCQ2zxw5WpzvvSc8PKe9o8Nc8.MdgNd9KETvM482umbM9/ifAq',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'2011-10-17 06:37:29','2011-10-17 06:37:29','Saji Raju',1,NULL),(12,'dhanesh@idyllic-software.com','$2a$10$nuDUxpHK7kE0gU3ncn8Ddu2yvpD5DzP8PNn4i8XjH.gk/IEPrW4uS',NULL,NULL,NULL,10,'2011-10-20 06:43:02','2011-10-20 06:42:22','61.12.126.211','61.12.126.211','2011-10-17 06:37:34','2011-10-20 06:43:02','Dhanesh. T.S',1,NULL),(13,'neha@idyllic-software.com','$2a$10$SGxS2b3FrawY8f479h792u1zvu2iAK7cvaHPifPtxGnS5oasuyVJK',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'2011-10-17 06:37:41','2011-10-17 06:37:41','Neha Jain',1,NULL),(14,'supriya@idyllic-software.com','$2a$10$wkRcj8U.C0lbux39uK.BEuZnzXGdV7Uf.42YhSj22BHhvjMwGnLm.',NULL,NULL,NULL,2,'2011-10-17 11:00:34','2011-10-17 09:50:07','122.169.66.226','61.12.126.211','2011-10-17 06:37:48','2011-10-17 11:00:34','Supriya Mane',1,NULL),(15,'dkpd@idyllic-software.com','$2a$10$QkhKtHZIIf/0flUHCrLvfe3rPN7GHbTvQuZj4ojMgtJNXoVgVzCAe',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'2011-10-17 06:37:53','2011-10-17 06:37:53','Dhaval Kapadia',1,NULL),(16,'sanil@idyllic-software.com','$2a$10$qVKtuAkHYwA0hlFkzNRpOuEvs8etQkOqC5.05L/2JxMrR2LN3/Z26',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'2011-10-17 06:38:00','2011-10-17 06:38:00','Sanil Joseph',1,NULL),(17,'rlee@idyllic-software.com','$2a$10$W8qqOBYmJVNy4Bd/Y5l/FufAeHmszwIZfiXEQ.XoGA3Lr0WCQ/hzW',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'2011-10-17 06:38:07','2011-10-17 06:38:07','Regan Lee',1,NULL),(18,'uma@idyllic-software.com','$2a$10$UMz9koanqL0j2bnwOuMCH.vQJi/c7nGu32RG.SJRAhUuqujjkefOC',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'2011-10-17 06:38:14','2011-10-17 06:38:14','Uma Kiron',1,NULL),(19,'dparekh@idyllic-software.com','$2a$10$gnnuTR6aASqzQ.7iVVIHoOKnHFFOeugjEMOe7O9aWrsw1fnjlRLBi',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'2011-10-17 06:38:21','2011-10-17 06:38:21','Deepali Parekh',1,NULL),(20,'vanita@idyllic-software.com','$2a$10$Nxmb5Kw0V5qvwWuAnP8ywO5axt34fBtohzOPTDjsxE2./lASeCeuy',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'2011-10-19 10:01:47','2011-10-19 10:01:47','Vanita Kothari',0,NULL);
/*!40000 ALTER TABLE `mavericks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profiles`
--

DROP TABLE IF EXISTS `profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `maverick_id` int(11) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `skype` varchar(255) DEFAULT NULL,
  `zone_of_genius` varchar(255) DEFAULT NULL,
  `tech_tools` varchar(255) DEFAULT NULL,
  `residence` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `personal_email` varchar(255) DEFAULT NULL,
  `emergency_contact` text,
  `status` varchar(255) DEFAULT NULL,
  `status_update_timestamp` datetime DEFAULT NULL,
  `google_reader_feed_url` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `picture_file_name` varchar(255) DEFAULT NULL,
  `picture_content_type` varchar(255) DEFAULT NULL,
  `picture_file_size` int(11) DEFAULT NULL,
  `picture_updated_at` datetime DEFAULT NULL,
  `purpose_of_life` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profiles`
--

LOCK TABLES `profiles` WRITE;
/*!40000 ALTER TABLE `profiles` DISABLE KEYS */;
INSERT INTO `profiles` VALUES (1,1,'1978-11-07','Believer','jparekh78','Visionary + Cultivator','Ruby on rails, Sinatra, Padrino, Java, J2EE, Struts, Hibernate, Spring, Pivotal tracker, Github, jquery, Passenger','4 Marble Arch, Ganesh Khind Rd, Aundh, Pune 411007','9850060710','railsbench@gmail.com','Gaurav: 9819142191','Working on putting together the new ISI website.','2011-10-17 09:04:45','http://www.google.com/reader/shared/04805178215908802366','2011-10-17 09:04:28','2011-10-17 09:04:45','Jinesh.jpg','image/jpeg',26417,'2011-10-17 09:04:28',NULL),(2,3,'1986-02-06','Conjurer','chirantanatwork','Visionary','Ruby, Rails','A 401, Kumar Sahwas, Baner Pashan Link Road, Pashan, Pune 411021','9225306063','chirantan.rajhans@gmail.com','Varsha Rajhans - 9225306064','Working on Why Do Diet and Siddharth\'s idea of the collaboration tool','2011-10-17 09:44:39','http://www.google.com/reader/public/atom/user%2F14417025687878320201%2Fstate%2Fcom.google%2Fbroadcast','2011-10-17 09:43:58','2011-10-18 09:56:38','me_at_office.png','image/png',48709,'2011-10-18 09:56:38',NULL),(3,12,'1987-01-12','Designer \"MAYAVI\"','dhanesh3220','Builder',' Adobe Photoshop, Aftereffects, Corel Draw, Goldwave Sound Editor, Premiere Pro, Maya','\"Dhanya Niwas\", 90/846 Gokhalenagar, Pune 16','9881344799','dhanesh3220@gmail.com','02025650085','Video intro for ISI website','2011-10-17 09:56:58','http://psd.tutsplus.com/  ,http://cgtantra.com/','2011-10-17 09:53:02','2011-10-20 06:49:04','DTS.jpg','image/jpeg',12044,'2011-10-20 06:49:04',NULL),(4,14,'1987-07-15','Tester','supriya.dinde','Visionary','Selenium, Rational Robot, QTP','c/o R.S.Mane, Opposite to petrol pump, near Siddhivinayak snacks Center, Old Sangvi, Pune - 411037','9096295319','supsd15@gmail.com','Rajesh Mane - 9552930337\r\n','','2011-10-19 10:44:00','http://www.google.com/reader/public/atom/user%2F18369729387538194255%2Fstate%2Fcom.google%2Fbroadcast','2011-10-17 10:02:17','2011-10-19 10:53:35','supriya.png','image/png',45790,'2011-10-17 10:08:00',''),(5,6,'1984-08-08','Sr. Rails Developer','prem.nagalla','Builder','Ruby on Rails','Pune, India','+918796801605','prem.nagalla@gmail.com','G.Srinivasa Rao - +919850614972','','2011-10-19 11:54:30','http://www.google.com/reader/public/atom/user%2F02150870192044475726%2Fstate%2Fcom.google%2Fbroadcast','2011-10-17 10:15:58','2011-10-19 11:54:30','prem.png','image/png',39466,'2011-10-17 10:15:00',''),(6,4,'1986-05-20','Lead Developer','vignesh.v.s','Visionary','Ruby on Rails, .NET','Pune','9850040805','vignesh20@gmail.com','+91 832 2701404',NULL,NULL,'http://www.google.com/reader/public/atom/user%2F03903456808232570997%2Fstate%2Fcom.google%2Fbroadcast','2011-10-17 11:13:25','2011-10-17 11:13:25','vignesh.png','image/png',46593,'2011-10-17 11:13:25',NULL),(7,9,'1979-12-14','Rails Developer','joshianiket','CULTIVATOR','Ruby on Rails, Java','Building # B3, Flat # 803, Kumar parisar, Kothrud, Pune - 411038','+919922986868','joshianiket22@yahoo.com','+917350787388 (My Better half)',NULL,NULL,'http://www.google.com/reader/public/atom/user%2F16413682856921553181%2Fstate%2Fcom.google%2Fbroadcast','2011-10-17 11:54:09','2011-10-19 13:04:32','aniket.png','image/png',42626,'2011-10-17 11:54:09',NULL),(8,5,'1984-04-02','Developer','siddharth020484','Architect','Rails, XMPP, RabbitMQ, Javascript, RubyMine, Textmate, Facebook','Mumbai','9920837799','sid.ravichandran@gmail.com','Priyanka Nandi : 9619562578','BaseJumping','2011-10-17 12:38:44','http://xkcd.com/rss.xml','2011-10-17 12:38:24','2011-10-17 12:38:44','P1010241.jpg','image/jpeg',873962,'2011-10-17 12:38:23',NULL),(9,10,'1989-01-23','*','ratnadeepd','Architect','Rails, Sinatra','Sidharth Height, Flt. No. 201, Near Medipoint Hospital, Aundh.','8983163913','ratnadeepdeshmane@gmail.com','Aniket (friend) - 9096417101\r\nHome - 0233-2535297',NULL,NULL,'http://www.google.com/reader/shared/ratnadeepdeshmane','2011-10-17 16:12:12','2011-10-19 12:19:38','ratan.png','image/png',18431,'2011-10-19 12:19:38',NULL),(10,8,'1984-10-07','Lead Developer','chetant.better','Cultivator','Ruby, Ruby on rails, Html, Css, ','Pune','+919850958854','chetan.thakkar07@gmail.com','Brother : 8975753020',NULL,NULL,'https://plus.google.com/u/0/102368695340501684554/','2011-10-18 04:07:36','2011-10-18 04:07:36','chetan.png','image/png',40795,'2011-10-18 04:07:35',NULL),(11,8,'1984-10-07','Lead Developer','chetant.better','Cultivator','Ruby, Ruby on rails, Html, Css, ','Pune','+919850958854','chetan.thakkar07@gmail.com','Brother : 8975753020',NULL,NULL,'https://plus.google.com/u/0/102368695340501684554/','2011-10-18 04:08:38','2011-10-18 04:08:38','chetan.png','image/png',40795,'2011-10-18 04:08:30',NULL),(12,7,'1986-04-06','CodeNinja','santosh.wadghule','Architect','Ruby, Rails, Python, PHP','Pune','9960591240','santosh.wadghule@gmail.com','9860352988\r\n9850022790','Currently working on minerva project.','2011-10-18 17:53:49','http://twitter.com','2011-10-18 07:48:21','2011-10-18 19:06:54','santosh.png','image/png',36187,'2011-10-18 12:44:46',NULL);
/*!40000 ALTER TABLE `profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `maverick_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `screenshot_file_name` varchar(255) DEFAULT NULL,
  `screenshot_content_type` varchar(255) DEFAULT NULL,
  `screenshot_file_size` int(11) DEFAULT NULL,
  `screenshot_updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects`
--

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` VALUES (1,1,'Manage my life','http://www.managemylife.com',NULL,'','2011-10-17 09:16:24','2011-10-19 13:42:56','Life Management: Everything just got easier - ManageMyLife.com_1319031645076.png','image/png',785513,'2011-10-19 13:42:56'),(2,1,'NTT','http://www.ntt.com',NULL,'','2011-10-17 09:17:36','2011-10-19 13:44:22','CCA_1319027624389.png','image/png',192187,'2011-10-19 13:44:21'),(3,1,'FB','http://www.5f.com',NULL,'','2011-10-17 09:21:52','2011-10-19 14:00:01','goodmantest on Facebook_1319030673060.png','image/png',553462,'2011-10-19 14:00:01'),(5,3,'My Property Loss','http://www.mypropertyloss.com',NULL,'*SAAS product for insurance companies. Lets users file insurance claims. Any issurance company can get a dedicated site with it\'s own content, form validations, email notifications on the fly. This is managed through a sleek admin interface.*\r\n\r\n`ruby` `ruby-on-rails`\r\n\r\nMy role was to design and develop a framework for dynamic content management, email notification management and insurer site specific claim filing form validations etc. Migrating old data into the new database schema before the launch was also one of the major responsibilities. I lead the development for this project for the second phase.','2011-10-17 10:07:47','2011-10-17 11:23:23','mpl.png','image/png',129686,'2011-10-17 11:23:22'),(6,12,'PHP Camp.net','http://phpcamp.net/',NULL,'PHPCamp is place to share news, views and articles that are useful to PHP community.\r\nMy role: Complete graphics designing.','2011-10-17 10:23:27','2011-10-17 10:23:27','phpcamp_net.png','image/png',199499,'2011-10-17 10:23:26'),(7,12,'PALANDRI WINERY','http://www.palandri.com.au/',NULL,'Palandri Winery, a member of 3 Oceans Wine Company, is one of Australia\'s youngest and most dynamic wine companies.  In James Hallidays\' latest issue of Australian Wine Companion, the wine and winery was rated a prestigious 4 stars.','2011-10-17 10:29:14','2011-10-17 10:29:14','www_palandri_com_au.png','image/png',436149,'2011-10-17 10:29:07'),(9,3,'Parnunu','http://www.parnunu.com',NULL,'### Parnunu\r\n_A job portal for users to keep all employment related information including documents, images, videos in one place and have recruiters search for candidates based on required criteria._\r\n\r\n`ruby` `ruby-on-rails` `jquery`\r\n\r\nMy responsibility was to lead the final iteration of the project which dealt with improving of code and design developed in the previous iterations. The iteration saw implementation of critical merchant features like credit card payment, discounts and refunds','2011-10-17 11:11:21','2011-10-17 11:11:21','parnunu.png','image/png',382964,'2011-10-17 11:11:17'),(11,3,'Getting Control','http://gettingcontrol.com/',NULL,'*This is a RESTful web implementation of popular task management Mac application called Things based on GTD. The backend was powered by Ruby on Rails.*\r\n\r\n`ruby` `ruby-on-rails` `jquery`\r\n\r\nMy responsibility was to design and implement various features of the application such bin management, repeating tasks, delegation of tasks etc. I was also a product design consultant for the project focusing on usability features and user experience.','2011-10-17 11:19:22','2011-10-17 11:19:22','gc.png','image/png',183035,'2011-10-17 11:19:21'),(12,6,'Chemoleums wWebsite and  webapp','http://chemoleums.com/ and NA',NULL,'Oil factory maintenance Project','2011-10-17 12:15:21','2011-10-19 12:17:25','chemoleums.png','image/png',345146,'2011-10-19 12:17:25'),(14,6,'HallOfFame','http://halloffame.co.in/ && https://app.halloffame.co.in/',NULL,'Hotel website and internal maintenance project','2011-10-17 12:27:50','2011-10-19 12:18:06','hof.png','image/png',985109,'2011-10-19 12:18:06'),(17,6,'poorawareness','http://poorawareness.cc.ly/',NULL,'An NGO project, which let users to know about the poor people statistics over the world. It collects on behalf of those poor people and helps them to live a better life.','2011-10-17 12:49:57','2011-10-19 12:19:43','poorawareness.png','image/png',129663,'2011-10-19 12:19:42'),(18,6,'whydodiet','http://wdd.idyllic-software.com/',NULL,'Diet management project which brings Nutritionists and their clients together and lets people manage their fitness.(In Development.)','2011-10-17 12:53:30','2011-10-19 12:19:50','wdd.png','image/png',375410,'2011-10-19 12:19:49'),(19,6,'Equipment Hunters (EQH)','http://equipmenthunters.com/',NULL,'Online shopping project for Heavy Equipments in USA.(In Development)','2011-10-17 12:55:05','2011-10-19 12:22:09','eqh.png','image/png',943905,'2011-10-19 12:22:08'),(20,10,'Khabaree','http://khabaree.in',NULL,'Something is fishy. There is something wrong with that guy. Oh, that bag seems to be lying there for long. There could be any suspicion. It will be conveyed to right people in due time. One timely \'Khabar\' can save the lives of many!\r\n','2011-10-17 16:24:52','2011-10-17 16:24:52','khabaree.png','image/png',912737,'2011-10-17 16:24:51'),(21,5,'Mentoredge','http://www.mentoredge.com',NULL,'Mentoredge is a mentoring platform developed for the Center of Innovation, Incubation and Entrepreneurship at IIM, Ahmedabad that allowed seasoned veterans to guide new entrepreneurs in managing and running their businesses.','2011-10-18 09:06:01','2011-10-19 12:58:57','Screen Shot 2011-10-19 at 6.22.52 PM.png','image/png',948860,'2011-10-19 12:58:57'),(22,7,'Why Do Diet','',NULL,'A Ruby on Rails project. The concept is user can follow the nutritionist. Under nutritionist guidance, user will follow the recipes that nutritionist will share to the user to control the diet.','2011-10-18 18:39:02','2011-10-18 18:39:02',NULL,NULL,NULL,NULL),(23,7,'Tanish-Bilish','http://tanish-bilish.com',NULL,'Online business networking hub','2011-10-18 18:40:30','2011-10-18 18:42:41',NULL,NULL,NULL,NULL),(24,7,'Parksyde','http://parksyde.com',NULL,'Corporate Builder site.','2011-10-18 18:41:48','2011-10-18 18:41:48',NULL,NULL,NULL,NULL),(25,7,'USV','',NULL,'','2011-10-18 18:43:04','2011-10-18 18:43:04',NULL,NULL,NULL,NULL),(26,12,'Digital painting : THE BEAUTY','',NULL,'Digital painting of a beautiful lady done using software Abobe Photoshop','2011-10-19 12:56:44','2011-10-20 06:38:30','BEAUTY.jpg','image/jpeg',133109,'2011-10-20 06:38:30'),(27,5,'Indovina il prezzo','http://concorso.automobile.it',NULL,'The application was a game launched for a 60 day period which required users to guess the price of 10 cars, allowing 20 seconds for each car. The users were allowed one game a day for the duration of the contest and the winner was awarded a Fiat 500.','2011-10-19 13:02:23','2011-10-19 13:04:00','Screen Shot 2011-10-19 at 6.21.29 PM.png','image/png',530376,'2011-10-19 13:03:59'),(28,8,'LF','',NULL,'','2011-10-19 13:36:26','2011-10-19 13:36:26','_1319014108261.png','image/png',814151,'2011-10-19 13:36:25'),(29,8,'ParkSyde','',NULL,'','2011-10-19 13:38:31','2011-10-19 13:38:31','_1319031448439.png','image/png',646673,'2011-10-19 13:38:30'),(30,8,'Hadoop','',NULL,'','2011-10-19 13:39:20','2011-10-19 13:39:20','hadoop.png','image/png',61903,'2011-10-19 13:39:20'),(31,1,'REI','http://rippleeffectimages.com',NULL,'','2011-10-19 14:07:54','2011-10-19 14:07:54','Screenshot.png','image/png',495792,'2011-10-19 14:07:54'),(32,12,'Parksyde','',NULL,'Township project in Nashik','2011-10-20 06:07:39','2011-10-20 06:38:44','p2.jpg','image/jpeg',194232,'2011-10-20 06:38:44'),(33,12,'Alcalas Western wear','',NULL,'Western wear outlet\r\n','2011-10-20 06:09:15','2011-10-20 06:38:59','p3.jpg','image/jpeg',181486,'2011-10-20 06:38:58'),(34,12,'Libero Firrero','',NULL,'Blog site','2011-10-20 06:10:35','2011-10-20 06:39:11','p5.jpg','image/jpeg',142894,'2011-10-20 06:39:10'),(35,12,'Tame me','',NULL,'Controlling and monitoring your Food Habits','2011-10-20 06:11:55','2011-10-20 06:39:27','p4.jpg','image/jpeg',148232,'2011-10-20 06:39:27'),(36,12,'97Changes','',NULL,'Donating a Dollar a day for a cause','2011-10-20 06:13:10','2011-10-20 06:39:54','p6.jpg','image/jpeg',121207,'2011-10-20 06:39:53'),(37,12,'Good Food','',NULL,'Maintaining your health with proper diet','2011-10-20 06:14:35','2011-10-20 06:39:41','p7.jpg','image/jpeg',167134,'2011-10-20 06:39:40');
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recommendations`
--

DROP TABLE IF EXISTS `recommendations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recommendations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `maverick_id` int(11) DEFAULT NULL,
  `body` text,
  `publish` tinyint(1) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recommendations`
--

LOCK TABLES `recommendations` WRITE;
/*!40000 ALTER TABLE `recommendations` DISABLE KEYS */;
/*!40000 ALTER TABLE `recommendations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_migrations`
--

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
INSERT INTO `schema_migrations` VALUES ('20110927075229'),('20110927132239'),('20110927132240'),('20110927132241'),('20110929085258'),('20111004050628'),('20111004092530'),('20111004115220'),('20111005060314'),('20111005070802'),('20111011052849'),('20111011062144'),('20111011103923'),('20111013050616'),('20111013050850'),('20111013093908'),('20111013110355'),('20111018101658'),('20111020044741');
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_profiles`
--

DROP TABLE IF EXISTS `social_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `maverick_id` int(11) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `linkedin` varchar(255) DEFAULT NULL,
  `stackoverflow` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_profiles`
--

LOCK TABLES `social_profiles` WRITE;
/*!40000 ALTER TABLE `social_profiles` DISABLE KEYS */;
INSERT INTO `social_profiles` VALUES (1,1,'idyllicsoftware','http://www.facebook.com/#!/jparekh3','http://in.linkedin.com/pub/idyllic-software/19/b37/9a2','','2011-10-17 09:32:53','2011-10-19 12:56:24'),(2,3,'chirantan','http://www.facebook.com/chirantan.rajhans','http://in.linkedin.com/in/chirantanrajhans','http://stackoverflow.com/users/45942/chirantan','2011-10-17 10:10:03','2011-10-19 12:54:41'),(3,12,'dhanesh3220','http://www.facebook.com/profile.php?id=100000155899286','http://www.linkedin.com/profile/edit?trk=hb_tab_pro_top','','2011-10-17 10:17:31','2011-10-19 12:55:56'),(4,14,'dsupriya15','http://www.facebook.com/#!/supriya.dindemane','http://www.linkedin.com/profile/edit?trk=hb_tab_pro_top','','2011-10-17 11:41:47','2011-10-19 12:55:52'),(5,4,'vignesh20','http://www.facebook.com/vignesh20','http://in.linkedin.com/in/vigneshshanbhag','','2011-10-17 11:50:57','2011-10-19 12:55:47'),(6,6,'http://twitter.com/#!/premnagalla','http://facebook.com/premnagalla','http://in.linkedin.com/pub/prem-kumar/14/b42/b71','http://stackoverflow.com/users/524740/prem','2011-10-17 12:13:04','2011-10-17 12:13:04'),(7,5,'supersid','','','','2011-10-17 12:55:24','2011-10-19 10:51:05'),(8,9,'aniketjoshi','http://www.facebook.com/joshianiket22','http://in.linkedin.com/in/joshianiket','http://stackoverflow.com/users/999168/aniket-joshi','2011-10-17 13:12:59','2011-10-19 12:55:27'),(9,10,'rtdp','http://facebook.com/ratnadeep.deshmane','http://in.linkedin.com/in/ratnadeepdeshmane','http://stackoverflow.com/users/392345/rtdp','2011-10-17 16:21:38','2011-10-19 12:55:17'),(10,8,'chetanDT','http://www.facebook.com/chetandt','http://in.linkedin.com/in/chetant ','http://stackoverflow.com/users/528939/chetan','2011-10-18 04:12:12','2011-10-18 04:12:12'),(11,7,'mechanicles','http://facebook.com/santosh.wadghule','','','2011-10-18 18:45:31','2011-10-19 12:55:00');
/*!40000 ALTER TABLE `social_profiles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-10-20  7:58:16
