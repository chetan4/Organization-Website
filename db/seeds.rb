# Create a default user
AdminUser.create!(
  :email                 => 'admin@idyllic-software.com',
  :password              => 'passme',
  :password_confirmation => 'passme'
)


[{:name => 'Jinesh Parekh',:email => 'jparekh@idyllic-software.com'},
  {:name => 'Brian Tsuchia',:email => 'brian@startupguru.com'},
  {:name => 'Chirantan Rajhans',:email => 'chirantan@idyllic-software.com'},
  {:name => 'Vignesh Shanbhag',:email => 'vignesh@idyllic-software.com'},
  {:name => 'Siddharth Ravichandran',:email => 'siddharth@idyllic-software.com'},
  {:name => 'Prem Kumar',:email => 'prem@idyllic-software.com'},
  {:name => 'Santosh Wadghule',:email => 'santosh@idyllic-software.com'},
  {:name => 'Chetan Thakkar',:email => 'chetan@idyllic-software.com'},
  {:name => 'Aniket Joshi',:email => 'aniket@idyllic-software.com'},
  {:name => 'Ratnadeep Deshmane',:email => 'ratnadeep@idyllic-software.com'},
  {:name => 'Saji Raju',:email => 'saji@idyllic-software.com'},
  {:name => 'Dhanesh Sadasivan',:email => 'dhanesh@idyllic-software.com'},
  {:name => 'Neha Jain',:email => 'neha@idyllic-software.com'},
  {:name => 'Supriya Mane',:email => 'supriya@idyllic-software.com'},
  {:name => 'Dhaval Kapadia',:email => 'dkpd@idyllic-software.com'},
  {:name => 'Sanil Joseph',:email => 'sanil@idyllic-software.com'},
  {:name => 'Regan Lee',:email => 'rlee@idyllic-software.com'},
  {:name => 'Uma Kiron',:email => 'uma@idyllic-software.com'},
  {:name => 'Deepali Parekh',:email => 'dparekh@idyllic-software.com'},
].each_with_index do |maverick,index|
  Maverick.create!(
    :name                  => maverick[:name],
    :email                 => maverick[:email],
    :password              => 'passme',
    :password_confirmation => 'passme',
    :sort_order            => index
  )
end



