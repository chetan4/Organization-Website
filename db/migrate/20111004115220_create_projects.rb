class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.integer :maverick_id
      t.string :name
      t.string :url
      t.string :phone
      t.text :description
      t.timestamps
    end
  end
end
