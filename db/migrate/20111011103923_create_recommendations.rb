class CreateRecommendations < ActiveRecord::Migration
  def change
    create_table :recommendations do |t|
      t.integer :maverick_id
      t.text :body
      t.boolean :publish
      t.string :author

      t.timestamps
    end
  end
end
