class CreateJobInquiries < ActiveRecord::Migration
  def change
    create_table :job_inquiries do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.string :git_hub_url
      t.text :aspirations
      t.timestamps
    end
  end
end
