class AddSortOrderToMavericks < ActiveRecord::Migration
  def change
    add_column :mavericks, :sort_order, :integer
  end
end
