class CreateClientInquiries < ActiveRecord::Migration
  def change
    create_table :client_inquiries do |t|
      t.string :name
      t.string :email
      t.text :message
      t.timestamps
    end
  end
end
