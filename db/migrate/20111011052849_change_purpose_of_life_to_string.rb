class ChangePurposeOfLifeToString < ActiveRecord::Migration
  def up
    change_column(:brief_bios, :purpose_of_life, :string)
  end

  def down
    change_column(:brief_bios, :purpose_of_life, :text)
  end
end
