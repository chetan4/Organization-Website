class AddEducationToBriefBio < ActiveRecord::Migration
  def change
    add_column :brief_bios, :education, :text
  end
end
