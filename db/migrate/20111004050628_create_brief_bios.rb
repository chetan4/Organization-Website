class CreateBriefBios < ActiveRecord::Migration
  def self.up
    create_table :brief_bios do |t|
      t.integer :maverick_id
      t.text :professional_experience
      t.text :purpose_of_life
      t.text :topics_of_interest
      t.text :why_work_with_me
      t.string :favorite_quote
      t.timestamps
    end
  end
  
  def self.down
    drop_table :brief_bios
  end
  
end
