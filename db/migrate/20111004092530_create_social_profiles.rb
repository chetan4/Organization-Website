class CreateSocialProfiles < ActiveRecord::Migration
  def change
    create_table :social_profiles do |t|
      t.integer :maverick_id
      t.string :twitter
      t.string :facebook
      t.string :linkedin
      t.string :stackoverflow

      t.timestamps
    end
  end
end
