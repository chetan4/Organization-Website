class AddPublishBooleanToMavericks < ActiveRecord::Migration
  def self.up
    add_column :mavericks, :publish, :boolean, :default => false
  end
  
  def self.down
    remove_column :mavericks, :publish
  end
end
