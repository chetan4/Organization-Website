class DeviseCreateMavericks < ActiveRecord::Migration
  def self.up
    create_table(:mavericks) do |t|
      t.database_authenticatable :null => false
      t.recoverable
      t.rememberable
      t.trackable

      # t.encryptable
      # t.confirmable
      # t.lockable :lock_strategy => :failed_attempts, :unlock_strategy => :both
      # t.token_authenticatable

      t.timestamps
      
      t.string :name
    end

    add_index :mavericks, :email,                :unique => true
    add_index :mavericks, :reset_password_token, :unique => true
    # add_index :mavericks, :confirmation_token,   :unique => true
    # add_index :mavericks, :unlock_token,         :unique => true
    # add_index :mavericks, :authentication_token, :unique => true
  end

  def self.down
    drop_table :mavericks
  end
end
