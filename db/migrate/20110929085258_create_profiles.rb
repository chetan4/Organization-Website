class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.integer :maverick_id
      t.date :dob
      t.string :position
      t.string :skype
      t.string :zone_of_genius
      t.string :tech_tools
      t.string :residence
      t.string :phone
      t.string :personal_email
      t.text :emergency_contact
      
      t.string :status
      t.datetime :status_update_timestamp
      
      t.string :google_reader_feed_url

      t.timestamps
    end
  end
end
