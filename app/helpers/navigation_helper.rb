module NavigationHelper

  def classify(menu_item)
    return 'selected'if controller_name == menu_item
    return''
  end

  def ie?
    user_agent = request.env['HTTP_USER_AGENT'].downcase
    return user_agent.match /msie/
  end
    
end
