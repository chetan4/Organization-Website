class PeopleController < SiteController
  def index
    @mavericks = Maverick.all_for_display
    @maverick = @mavericks.first
    @maverick = Maverick.find(params[:maverick]) if params[:maverick]
  end

   def show
    @maverick = Maverick.find(params[:id])
    render :partial => 'home/profile_information'
  end
end
