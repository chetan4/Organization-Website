class CareersController < SiteController

  def index
    redirect_to :action => 'new'
  end

  def new
    @job_inquiry = JobInquiry.new
  end

  def create
    @job_inquiry = JobInquiry.new(params[:job_inquiry])
    human = verify_recaptcha
    if human && @job_inquiry.save
      render :template => 'careers/thankyou'
    else
      @job_inquiry.valid?
      @job_inquiry.errors.add(:base, 'You failed to identify as human') if !human
      render :template => 'careers/new'
    end
  end
end
