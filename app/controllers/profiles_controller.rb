class ProfilesController < ApplicationController
  layout "application", :except => [:preview]
  before_filter :except => [:new, :create] do
    @profile = current_maverick.profile
    unless @profile
      redirect_to new_profile_path
    end
  end
  
  def show
  end
  
  def new
    redirect_to profiles_path if current_maverick.profile
    @profile = Profile.new
  end
  
  def create
    @profile = Profile.new(
      {
        :maverick_id => current_maverick.id
      }.merge(params[:profile])
    )
    if @profile.save
      redirect_to profile_path
    else
      render :action => :new
    end
  end
  
  def update
    if @profile.update_attributes(params[:profile])
      redirect_to profile_path
    else
      render :action => :edit
    end
  end

  def preview
    @mavericks = Maverick.all_for_display
    @maverick = current_maverick
    render :layout => "site", :template => "home/index"
  end

end
