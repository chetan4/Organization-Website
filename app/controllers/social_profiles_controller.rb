class SocialProfilesController < InheritedResources::Base
  
  before_filter :except => [:new, :create] do
    @social_profile = current_maverick.social_profile
    unless @social_profile
      redirect_to new_social_profile_path
    end
  end
  
  def show
  end
  
  def new
    redirect_to social_profile_path if current_maverick.social_profile
    @social_profile = SocialProfile.new
  end
  
  def create
    @social_profile = SocialProfile.new(
      {
        :maverick_id => current_maverick.id
      }.merge(params[:social_profile])
    )
    if @social_profile.save
      redirect_to social_profile_path
    else
      render :action => :new
    end
  end
  
  def update
    if @social_profile.update_attributes(params[:social_profile])
      redirect_to social_profile_path
    else
      render :action => :edit
    end
  end
  
end
