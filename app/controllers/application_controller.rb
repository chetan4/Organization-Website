class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :authenticate_maverick!, :unless => :under_admin?
  
  def under_admin?
    request.url.include? 'admin'
  end
end
