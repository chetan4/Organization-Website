class BlogController < SiteController
  
  def index
    @feed = Feedzirra::Feed.fetch_and_parse(SiteConfig.blog.feed_url)    
  end
  
end
