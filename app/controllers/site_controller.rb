class SiteController < ApplicationController
  skip_filter :authenticate_maverick!
  before_filter :redirect_if_maverick
  layout 'site'

  def redirect_if_maverick
    redirect_to '/profile' if current_maverick
  end
end
