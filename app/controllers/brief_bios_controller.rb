class BriefBiosController < InheritedResources::Base
  
  before_filter :except => [:new, :create] do
    @brief_bio = current_maverick.brief_bio
    unless @brief_bio
      redirect_to new_brief_bio_path
    end
  end
  
  def show
  end
  
  def new
    redirect_to brief_bio_path if current_maverick.brief_bio
    @brief_bio = BriefBio.new
  end
  
  def create
    @brief_bio = BriefBio.new(
      {
        :maverick_id => current_maverick.id
      }.merge(params[:brief_bio])
    )
    if @brief_bio.save
      redirect_to brief_bio_path
    else
      render :action => :new
    end
  end
  
  def update
    if @brief_bio.update_attributes(params[:brief_bio])
      redirect_to brief_bio_path
    else
      render :action => :edit
    end
  end
  
end
