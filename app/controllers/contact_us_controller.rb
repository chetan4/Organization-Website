class ContactUsController < SiteController

  def index
    redirect_to :action => :new
  end  
  
  def new
    @client_inquiry = ClientInquiry.new
  end
  
  def create
    @client_inquiry = ClientInquiry.new(params[:client_inquiry])
    if @client_inquiry.save
      render :template => 'contact_us/thankyou'
    else      
      render :template => 'contact_us/new'
    end
  end

end
