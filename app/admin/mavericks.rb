ActiveAdmin.register Maverick do
  
  index do
    column :name do |maverick|
      link_to maverick.name, admin_maverick_path(maverick)
    end
    column :email do |maverick|
      link_to maverick.email, "mailto:#{maverick.email}"
    end
    column :published do |maverick|
      maverick.publish.to_s
    end
    
    column "Edit" do |maverick|
      link_to image_tag("edit.png"), edit_admin_maverick_path(maverick), :title => "edit"
    end
    column "Delete" do |maverick|
      link_to image_tag("delete.png"), admin_maverick_path(maverick), :title => "delete"
    end
  end
  
  
  form do |f|
    f.inputs "Who's the maverick?" do
      f.input :email
      f.input :name
      f.input :publish
      f.input :password
      f.input :password_confirmation, :required => true
      f.buttons
    end
  end
  
  show do
    h1 maverick.name
    h2 maverick.email
  end
  
end
