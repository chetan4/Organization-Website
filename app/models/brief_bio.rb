
class BriefBio < ActiveRecord::Base
  belongs_to :maverick
  
  validates :professional_experience, :presence => true
  validates :purpose_of_life,         :presence => true
  validates :topics_of_interest,      :presence => true
  validates :why_work_with_me,        :presence => true
  validates :favorite_quote,          :presence => true
    
  extend_attribute :purpose_of_life, :extend => [WordMethods, MarkdownMethods]
  extend_attribute :professional_experience, :extend => [WordMethods, MarkdownMethods]
  extend_attribute :education, :extend => [WordMethods, MarkdownMethods]
  extend_attribute :favorite_quote, :extend => [WordMethods, MarkdownMethods]
  extend_attribute :topics_of_interest, :extend => [WordMethods, MarkdownMethods]
  extend_attribute :why_work_with_me, :extend => [WordMethods, MarkdownMethods]
    
end
