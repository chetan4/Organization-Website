class Maverick < ActiveRecord::Base
  devise :database_authenticatable,
    :recoverable, :rememberable, :trackable, :validatable
  #:token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable, :omniauthable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :name, :email, :publish, :password, :password_confirmation, :remember_me
  
  validate :name, :presence => true
  
  has_one :profile
  has_one :brief_bio
  has_one :social_profile
  has_many :projects
  has_many :recommendations

  scope :all_for_display, where(:publish => true).order("sort_order")

  def has_picture?
    profile && profile.picture
  end

  def displayable_projects
    result = []
    projects.each do |project|
      result << project if project.screenshot
    end
    return result
  end

  def has_displayable_projects?
    projects.each do |project|
      return true if project.screenshot
    end
    return false
  end

  def status
    profile.blank? ? "" : profile.status
  end

  def personal_email
    profile.blank? ? "" : profile.personal_email
  end

  def age
    Date.today.year - profile.dob.year
  end

  def picture
    profile.picture
  end
  
  def position
    profile.position
  end

  def skype
    profile.skype
  end
  
  def tech_tools
    profile.tech_tools
  end

  def zone_of_genius
    profile.zone_of_genius
  end

  def twitter
    social_profile.blank? ? "#" : social_profile.twitter
  end

  def linkedin
    linkedin_url = social_profile.blank? ? "" : social_profile.linkedin
    if !linkedin_url.blank? && linkedin_url.include?("http")
      linkedin_url
    else
      "http://#{linkedin_url}"
    end
  end
  
  def facebook
    facebook_url = social_profile.blank? ? "" : social_profile.facebook
    if facebook_url.blank? && facebook_url.include?("http")
      facebook_url
    else
      "http://#{facebook_url}"
    end
  end


  
end
