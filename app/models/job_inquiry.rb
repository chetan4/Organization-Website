class JobInquiry < ActiveRecord::Base
  validates :name, :presence => true
  
  validates :email, :presence => true,
    :format => {:with => %r{^(?:[_a-z0-9-]+)(\.[_a-z0-9-]+)*@([a-z0-9-]+)(\.[a-zA-Z0-9\-\.]+)*(\.[a-z]{2,4})$}i}
  
  has_attached_file :resume,
    :storage => :s3,
    :s3_credentials => {
    :access_key_id     => SiteConfig.s3.access_key_id,
    :secret_access_key => SiteConfig.s3.secret_access_key},
    :path   => ":attachment/:id/:style/:basename.:extension",
    :bucket => SiteConfig.s3.bucket
  validates_attachment_size :resume, :less_than => 5.megabytes

  after_save :notify

  def notify
    JobInquiryMailer.notify(self).deliver
  end

end
