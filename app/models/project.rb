class Project < ActiveRecord::Base
  validates :name, :presence => true

  belongs_to :maverick

  

  has_attached_file :screenshot,
    :storage => :s3,
    :s3_credentials => {
    :access_key_id     => SiteConfig.s3.access_key_id,
    :secret_access_key => SiteConfig.s3.secret_access_key
  },
    :path   => ":attachment/:id/:style/:basename.:extension",
    :bucket => SiteConfig.s3.bucket,
    :styles => { :medium => "640X472"}
    validates_attachment_presence :screenshot


  Paperclip.interpolates :normalized_file_name do |attachment, style|
    attachment.instance.normalized_file_name
  end

  def normalized_file_name
    "#{self.id}-#{self.screenshot_file_name.gsub( /[^a-zA-Z0-9_\.]/, '_')}" 
  end
  
  extend_attribute :description, :extend => MarkdownMethods
  
end
