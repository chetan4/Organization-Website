class ClientInquiry < ActiveRecord::Base

  validates :name, :presence => true
  validates :message, :presence => true
  validates :email, :presence => true,
    :format => {:with => %r{^(?:[_a-z0-9-]+)(\.[_a-z0-9-]+)*@([a-z0-9-]+)(\.[a-zA-Z0-9\-\.]+)*(\.[a-z]{2,4})$}i}

  after_save :notify

  def notify
    ClientInquiryMailer.notify(self).deliver
  end
end
