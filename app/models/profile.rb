class Profile < ActiveRecord::Base
  belongs_to :maverick
  
  validates :dob,                     :presence => true
  validates :position,                :presence => true
  validates :zone_of_genius,          :presence => true
  validates :tech_tools,              :presence => true
  validates :residence,               :presence => true
  validates :phone,                   :presence => true
  validates :emergency_contact,       :presence => true
  validates :google_reader_feed_url,  :presence => true
  
  before_update :set_status_update_timestamp
  
  has_attached_file :picture,
    :storage => :s3,
    :s3_credentials => {
      :access_key_id     => SiteConfig.s3.access_key_id,
      :secret_access_key => SiteConfig.s3.secret_access_key
    },
    :path   => ":attachment/:id/:style/:basename.:extension",
    :bucket => SiteConfig.s3.bucket,
    :styles => { :medium => "149", :thumb => "38" }
    
  Paperclip.interpolates :normalized_file_name do |attachment, style|
    attachment.instance.normalized_file_name
  end

  def normalized_file_name
    "#{self.id}-#{self.picture_file_name.gsub( /[^a-zA-Z0-9_\.]/, '_')}" 
  end
  
  def tech_tools;  RDiscount.new(super) end
  
  def set_status_update_timestamp
    if status_changed?
      self.status_update_timestamp = Time.now
    end
  end
  
  def recent_readings(limit = 4)
    feed = Feedzirra::Feed.fetch_and_parse(google_reader_feed_url)
    feed.entries.collect do |entry|
      {
        :url     => entry.url,
        :title   => entry.title
      }
    end[0...limit] rescue []
  end
  
end
