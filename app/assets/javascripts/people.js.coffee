# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$ ->
  $('.myWorkNavigation').click ->
    setTimeout('initializeProjectCarousel()',1000)
    $('.myWorkNavigation img').attr('src', '/assets/myWorkActive.png')
    $('.briefBioNavigation img').attr('src', '/assets/briefBioInactive.png')
    $('.projects').show()
    $('.maverickInformation').hide()


  $('.briefBioNavigation').click ->
    $('.briefBioNavigation img').attr('src', '/assets/briefBioActive.png')
    $('.myWorkNavigation img').attr('src', '/assets/myWorkInactive.png')
    $('.maverickInformation').show()
    $('.projects').hide()


  $("#teamPictures").jCarouselLite
    btnNext: ".teamPicturesNext",
    btnPrev: ".teamPicturesBack",
    visible: 6,
    circular: false,
    speed: 500,
    scroll: 1,
    afterEnd: (args)->
      hasFirstImage = $($(args[0])).hasClass('first')
      hasLastImage = $($(args[args.length - 1])).hasClass('last')
      if (hasLastImage)
        nextImage = '/assets/nextOff.png'
      else
        nextImage = '/assets/nextOn.png'

      if (hasFirstImage)
        backImage = '/assets/backOff.png'
      else
        backImage = '/assets/backOn.png'

      $('.teamPicturesBack img').attr('src', backImage)
      $('.teamPicturesNext img').attr('src', nextImage)
      return true

  $(".maverick").click ->
    image = $(this).children('img')[0]
    url = $(image).attr('src')
    $(image).attr('src','/assets/spinner.gif')
    maverickId = $(this).attr('id');
    $('.maverick').removeClass('selected')
    $('#' + maverickId).addClass('selected')
    $.get('/people/show/'+maverickId, (data) ->
      $('#maverickProfile').html(data)
      $(image).attr('src',url)
      $('.briefBioNavigation img').attr('src', '/assets/briefBioActive.png')
      $('.myWorkNavigation img').attr('src', '/assets/myWorkInactive.png')
    )


  $(".tipTip").tipTip({ position: "top center",attribute: 'name-data'})

