class JobInquiryMailer < ActionMailer::Base
  default from: "commons@idyllic-software.com"

  def notify(inquiry)
    @inquiry = inquiry
    mail(:to => 'jparekh@idyllic-software.com', :subject => "A new job application has come in.")
  end
end
