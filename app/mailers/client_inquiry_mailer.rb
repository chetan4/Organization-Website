class ClientInquiryMailer < ActionMailer::Base
  default from: "idyllic.software@gmail.com"

  def notify(inquiry)
    @inquiry = inquiry
    mail(:to => 'jparekh@idyllic-software.com', :subject => "A new client inquiry has come in.")
  end
end
