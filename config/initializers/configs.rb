class SiteConfig < ConfigReader
  self.config_file = File.join(Rails.root, "config", "site_config.yml")
end