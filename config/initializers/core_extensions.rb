require 'rdiscount'

class RDiscount
  def to_s
    @text.to_s
  end
end
=begin
module ActiveRecord
  class Base
    class << self
      def extend_attribute(attr, options = {}, &block)
        after_initialize ExtendAttribute.new(attr, options, block)
        after_find       ExtendAttribute.new(attr, options, block)
      end
    end

    class ExtendAttribute

      def initialize(attr, options = {}, block = nil)
        @attr = attr
        @options = options
        @block = block
      end

      def after_initialize(record)
        modules = [*@options[:extend]]
        modules << Module.new(&@block) if @block
        modules.each do |entend|
          record.send(@attr).send(:extend, entend)
        end
      end
      alias :after_find :after_initialize

    end
  end
end
=end